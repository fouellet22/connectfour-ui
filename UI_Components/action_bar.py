import os

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image


class ActionBarWidget(BoxLayout):
    """ Display as much action arrows as there are grid columns """
    def __init__(self, on_action_touch_down, cols):
        super(ActionBarWidget, self).__init__()
        self.on_action_touch_down = on_action_touch_down
        self.actions = {}
        for column in range(cols):
            action = ActionArrow(self.on_action_touch_down, column)
            self.actions[column] = action
            self.add_widget(action)

    def enable_actions(self, actions):
        """
        Takes and array of booleans, one per action arrow,
        and enables them accordingly
        """
        if actions is None:
            for i in self.actions:
                self.actions[i].set_enabled(False)
        else:
            for i, v in enumerate(actions):
                self.actions[i].set_enabled(v)


class ActionArrow(Image):
    """ Display an index-aware arrow and detect user touch down input """
    def __init__(self, on_action_touch_down, column):
        super(ActionArrow, self).__init__()
        self.enabled_img = os.path.join('.', 'res', 'img', 'arrow.png')
        self.disabled_img = os.path.join('.', 'res', 'img', 'square_empty.png')
        self.source = self.disabled_img
        self.on_action_touch_down = on_action_touch_down
        self.column = column

    def set_enabled(self, enabled):
        """ Disable and hide, or enable and show """
        self.disabled = not enabled
        self.source = self.enabled_img if enabled else self.disabled_img

    def on_touch_down(self, touch):
        """ Call parent function if properly touched down """
        if not self.disabled and self.collide_point(*touch.pos):
            self.on_action_touch_down(self.column)
        return super(ActionArrow, self).on_touch_down(touch)
