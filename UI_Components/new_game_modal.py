import os

from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.modalview import ModalView
from kivy.uix.togglebutton import ToggleButton


class NewGameModal(ModalView):
    """ Modal where the players can be set either to humans or machines """
    def __init__(self,  on_close, auto_dismiss=True,):
        super(NewGameModal, self).__init__()
        self.size_hint = (0.65, 0.5)
        self.auto_dismiss = auto_dismiss
        self.on_close = on_close
        self.padding = 20

        self.player1 = PlayerTypeBox('Player 1')
        self.player2 = PlayerTypeBox('Player 2')

        modal_box = BoxLayout(orientation='vertical')

        players_box = BoxLayout()
        players_box.add_widget(self.player1)
        players_box.add_widget(self.player2)

        modal_box.add_widget(players_box)
        modal_box.add_widget(Button(size_hint=(0.25, 0.15), pos_hint={'center_x': 0.5},
                                    text="Start game", on_press=self.start_game))
        self.add_widget(modal_box)

    def start_game(self, e):
        """ Call the parent component function when the button is pressed """
        self.on_close(self.player1.is_human(), self.player2.is_human())


class PlayerTypeBox(BoxLayout):
    """ A player (human|machine) toggle box """
    def __init__(self, name):
        super(PlayerTypeBox, self).__init__()
        self.orientation = 'vertical'

        self.btn_human = ToggleImage(name=name, state='down', source=os.path.join('.', 'res', 'img', 'human.png'))
        self.btn_machine = ToggleImage(name=name, state='normal', source=os.path.join('.', 'res', 'img', 'ai.png'))

        box = BoxLayout(padding=20)
        box.add_widget(self.btn_human)
        box.add_widget(self.btn_machine)

        self.add_widget(Label(text=name, size_hint_y=0.25, font_size=24))
        self.add_widget(box)

    def is_human(self):
        """ Used to determine which player is human and which is machine """
        return self.btn_human.get_state() == 'down'


class ToggleImage(AnchorLayout):
    def __init__(self, name, state, source):
        super(ToggleImage, self).__init__()
        self.btn = ToggleButton(group=name, state=state, allow_no_selection=False)
        self.add_widget(self.btn)
        self.add_widget(Image(source=source))

    def get_state(self):
        return self.btn.state

