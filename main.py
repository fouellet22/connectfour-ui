from kivy import Config
from kivy.app import App

from game_ui import GameUI
from game_loop import GameLoop
from UI_Components.new_game_modal import NewGameModal


class ConnectFourApp(App):
    def __init__(self):
        super(ConnectFourApp, self).__init__()
        self.rows = 6
        self.cols = 7

        self.game_loop = GameLoop(rows=self.rows, cols=self.cols)
        self.modal = NewGameModal(auto_dismiss=False, on_close=self.start_game)

    def build(self):
        self.root = GameUI(rows=self.rows, cols=self.cols, player_act=self.game_loop.play_action,
                           on_new_game=self.game_loop.on_new_game, on_resign=self.game_loop.on_resign)
        return self.root

    def on_start(self):
        self.modal.open()

    def start_game(self, is_player1_human, is_player2_human):
        self.modal.dismiss()
        self.game_loop.start(self.root, is_player1_human, is_player2_human)


if __name__ == '__main__':
    Config.set('graphics', 'width', '1000')
    Config.set('graphics', 'height', '700')
    Config.set('graphics', 'resizable', False)
    Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
    Config.set('graphics', 'multisamples', '0')
    app = ConnectFourApp()
    app.run()
