from kivy.uix.floatlayout import FloatLayout

from UI_Components.action_bar import ActionBarWidget
from UI_Components.game_board import GameBoard
from UI_Components.menu import MenuWidget
from UI_Components.player_status import PlayerStatusWidget


class GameUI(FloatLayout):
    """ Display the visual parts of the game itself and handle human input """

    def __init__(self, rows, cols, player_act, on_new_game, on_resign):
        super(GameUI, self).__init__()
        self.rows = rows
        self.cols = cols
        self.player_act = player_act

        # The layout is split in 4:
        #   Left:         Board
        #   Top-Left:     Action bar (display legal moves on human turn)
        #   Top-Right:    Player status (portraits, points, turn)
        #   Bottom-Right: Menu (new game and resign buttons)
        self.game_board = GameBoard(rows=self.rows, cols=self.cols, on_square_touch_down=self.on_action_touch_down)
        self.action_bar = ActionBarWidget(self.on_action_touch_down, cols=cols)
        self.player_status = PlayerStatusWidget()
        self.menu = MenuWidget(on_resign=on_resign, on_new_game=on_new_game)
        self.add_widget(self.game_board)
        self.add_widget(self.action_bar)
        self.add_widget(self.player_status)
        self.add_widget(self.menu)

    def on_action_touch_down(self, action_column):
        self.player_act(action_column)

    def render_board(self, board, index):
        self.game_board.update_board(board, index)

    def highlight_player(self, player):
        self.player_status.highlight(player)

    def enable_actions(self, actions):
        self.action_bar.enable_actions(actions)
        self.game_board.enable_actions(actions)

    def enable_resign(self):
        self.menu.enable_resign()

    def disable_resign(self):
        self.menu.disable_resign()

    def enable_menu(self):
        self.menu.enable()

    def disable_menu(self):
        self.menu.disable()

    def declare_winner(self, winner):
        self.player_status.declare_winner(winner)
        self.stop_game()

    def declare_draw(self):
        self.player_status.declare_draw()
        self.stop_game()

    def stop_game(self):
        self.game_board.disabled = True
        self.action_bar.disabled = True

    def restart_game(self):
        self.player_status.declare_winner(None)
        self.game_board.disabled = False
        self.action_bar.disabled = False
