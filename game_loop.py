import os

import numpy as np
from keras.models import load_model
from kivy.clock import Clock

from examples.connect_four.connect_four_environment import ConnectFourEnvironment
from examples.connect_four.connect_four_game_status import ConnectFourGameStatus
from examples.connect_four.connect_four_machine_agent import ConnectFourMachineAgent
from examples.connect_four.connect_four_model import ConnectFourModel

BEST_MODEL_PATH = os.path.join('..', 'alpha-zero', 'models', 'best_model.h5')
NUM_SIMULATIONS = 1000
Clock.max_iteration = 1000


class GameLoop:
    """ Handles the state of the game; Controls the UI and queries the Environment """
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.is_player_human = [True, True]

        self.ui = None
        self.env = ConnectFourEnvironment()
        self.state = self.env.gen_init_state()

        best_model = ConnectFourModel(load_model(BEST_MODEL_PATH if os.path.exists(BEST_MODEL_PATH) else None))
        self.machines = [ConnectFourMachineAgent(best_model, NUM_SIMULATIONS, 0.0),
                         ConnectFourMachineAgent(best_model, NUM_SIMULATIONS, 0.0)]

    def start(self, ui, is_player_1_human, is_player_2_human):
        """ Start (or restart) a new game with new player parameters """
        self.ui = ui
        self.is_player_human = [is_player_1_human, is_player_2_human]

        # This part is a quick fix to prevent screen freezing
        # when the first player is a machine
        self.ui.disable_menu()
        self.ui.enable_actions(None)
        self.ui.highlight_player(0)
        Clock.schedule_once(self.update_callback, 1)

    def update_callback(self, _):
        """ Same as update, but as a callback function """
        self.update()

    def update(self):
        """ Set the UI for a human or the machine and make machine play if applicable """
        status = self.env.check_game_status(self.state)
        if status == ConnectFourGameStatus.P1WIN:
            self.ui.declare_winner(1)
            self.ui.enable_menu()
            self.ui.disable_resign()
        elif status == ConnectFourGameStatus.P2WIN:
            self.ui.declare_winner(2)
            self.ui.enable_menu()
            self.ui.disable_resign()
        elif status == ConnectFourGameStatus.DRAW:
            self.ui.declare_draw()
            self.ui.enable_menu()
            self.ui.disable_resign()
        else:
            turn = self.state.initiative - 1
            self.ui.highlight_player(turn)
            is_player_human = self.is_player_human[turn]
            if is_player_human:
                self.update_for_human()
            else:
                self.update_for_machine()

    def update_for_human(self):
        """ Change the UI so that the player can play """
        # TODO: Change how legal actions work with action bar
        legal_actions = self.get_legal_actions(self.state.board)
        self.ui.enable_actions(legal_actions)
        self.ui.enable_menu()

    def update_for_machine(self):
        """ Change the UI so that the player can NOT play and make the machine play """
        self.ui.enable_actions(None)
        self.ui.disable_menu()
        # Wait a frame for the last render to take effect
        Clock.schedule_once(self.machine_callback, 0)

    def machine_callback(self, _):
        """ Callback function that calls the machine's act function """
        action = self.machines[self.state.initiative - 1].act(self.state)
        self.play_action(action)

    def play_action(self, action):
        """ Either by the player or by the machine, take an action and apply it to the board """
        row = self.get_playable_square(self.state.board, action)

        # An illegal move should not happen; if so, just ignore it and replay the turn
        if row < 0:
            self.update()
            return

        self.state = self.env.apply_action(self.state, action)
        self.ui.render_board(self.state.board, self.reversed_2d_to_1d_index(row, action))
        self.update()

    def on_new_game(self, is_player_1_human, is_player_2_human):
        """ Clear the state, the board and start again with new player parameters """
        self.reset_state()
        self.ui.render_board(self.state.board, None)
        self.ui.restart_game()
        self.start(self.ui, is_player_1_human, is_player_2_human)

    def reset_state(self):
        """ Clear the board and the players """
        self.state = self.env.gen_init_state()

    def on_resign(self, e):
        """ Makes the other player the winner (only callable by a human) """
        turn = self.state.initiative - 1
        if self.is_player_human[turn]:
            self.ui.declare_winner(self.next_turn(turn) + 1)
            self.ui.disable_resign()

    @staticmethod
    def next_turn(turn):
        return (turn + 1) % 2

    def reversed_2d_to_1d_index(self, row, col):
        return col + (self.rows - row) * self.rows - row - 1

    def get_legal_actions(self, board):
        """ Check what moves (columns) are playable on the board """
        return [self.get_playable_square(board, x) >= 0 for x in range(self.cols)]

    @staticmethod
    def get_playable_square(board, col):
        """ Find the square that will be filled by playing of the given column on the board """
        empty_squares = np.where((board[:, col]) == 0)[0]
        if len(empty_squares > 0):
            return empty_squares[0]
        else:
            return -1  # TODO: Maybe manage errors in a better way?

