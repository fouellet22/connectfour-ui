import os

from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image

from UI_Components.new_game_modal import NewGameModal


class MenuWidget(BoxLayout):
    """ Show buttons for general game controls """

    def __init__(self, on_resign, on_new_game):
        super(MenuWidget, self).__init__()
        self.on_resign = on_resign
        self.on_new_game = on_new_game
        self.modal = NewGameModal(on_close=self.start_new_game)
        self.spacing = 20

        self.new_game_btn = ImageButton(source=os.path.join('.', 'res', 'img', 'new_game.png'), on_press=self.new_game)
        self.resign_btn = ImageButton(source=os.path.join('.', 'res', 'img', 'resign.png'),
                                      on_press=self.on_resign)

        self.add_widget(self.new_game_btn)
        self.add_widget(self.resign_btn)

    def new_game(self, e):
        """ Open the same option menu as when the program is opened """
        self.modal.open()

    def start_new_game(self, is_player_1_human, is_player_2_human):
        """ Notify the parent component that a new game was asked (with new parameters) """
        self.modal.dismiss()
        self.on_new_game(is_player_1_human, is_player_2_human)

    def enable_resign(self):
        self.resign_btn.disabled = False

    def disable_resign(self):
        self.resign_btn.disabled = True

    def enable(self):
        self.resign_btn.disabled = False
        self.new_game_btn.disabled = False

    def disable(self):
        self.resign_btn.disabled = True
        self.new_game_btn.disabled = True


class ImageButton(AnchorLayout):
    """ A button with an image in its center instead of text """

    def __init__(self, source, on_press):
        super(ImageButton, self).__init__()
        btn = Button(on_press=on_press)
        btn.background_normal = ''
        btn.background_color = (0.62, 0.86, 0.96, 0)
        btn.size_hint = (1.1, 1)
        self.add_widget(btn)
        self.add_widget(Image(source=source))
