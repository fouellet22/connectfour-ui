import os

from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.label import Label


class PlayerStatusWidget(BoxLayout):
    """ Show the avatar or both players and highlight the one whose turn it is """
    def __init__(self):
        super(PlayerStatusWidget, self).__init__()
        self.winner_text = Label(text="", size_hint_y=0.5, font_size=24)
        self.add_widget(self.winner_text)

        self.players = [PlayerCard(), PlayerCard()]
        self.add_widget(self.players[0])
        self.add_widget(self.players[1])

    def highlight(self, player):
        """ Highlight a player and unhighlight the other """
        self.players[0].unhighlight()
        self.players[1].unhighlight()
        self.players[player].highlight()

    def declare_winner(self, winner):
        """ Display a win message or removes it """
        if winner is None:
            self.winner_text.text = ""
        else:
            self.winner_text.text = f'Player {winner} Wins!'

    def declare_draw(self):
        self.winner_text.text = f'It is a draw!'


class PlayerCard(AnchorLayout):
    """ A player's avatar and its highlight layer """
    def __init__(self):
        super(PlayerCard, self).__init__()
        self.card = Image(source=os.path.join('.', 'res', 'img', 'player_card.png'))
        self.highlighted = os.path.join('.', 'res', 'img', 'player_image_highlight.png')
        self.unhighlighted = os.path.join('.', 'res', 'img', 'player_image.png')

        self.card_foreground = Image(source=self.unhighlighted)

        self.add_widget(self.card)
        self.add_widget(self.card_foreground)

    def highlight(self):
        self.card_foreground.source = self.highlighted
        self.card_foreground.size_hint = (1, 1)

    def unhighlight(self):
        self.card_foreground.source = self.unhighlighted
        self.card_foreground.size_hint = (0.75, 0.75)
