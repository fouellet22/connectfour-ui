import os

import numpy as np
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image

IMG_SRC = {
    0: os.path.join('.', 'res', 'img', 'square_empty.png'),
    1: os.path.join('.', 'res', 'img', 'square_p1.png'),
    2: os.path.join('.', 'res', 'img', 'square_p2.png'),
    'highlight': os.path.join('.', 'res', 'img', 'square_highlight.png'),
    'background': os.path.join('.', 'res', 'img', 'board_background.png'),
    'foreground': os.path.join('.', 'res', 'img', 'board_foreground.png')
}


class GameBoard(FloatLayout):
    """ Group the background, foreground and grid together (with a highlight grid) """
    def __init__(self, rows, cols, on_square_touch_down):
        super(GameBoard, self).__init__()
        self.background = Image(pos=(20, 20), source=IMG_SRC['background'])
        self.pieces = PieceGrid(rows=rows, cols=cols, on_square_touch_down=on_square_touch_down)
        self.highlights = HighlightGrid(rows=rows, cols=cols)
        self.foreground = Image(pos=(20, 20), source=IMG_SRC['foreground'])

        self.add_widget(self.background)
        self.add_widget(self.pieces)
        self.add_widget(self.highlights)
        self.add_widget(self.foreground)

    def update_board(self, board, move):
        """ Update the piece distribution and highlights on the board """
        self.pieces.update_board(board)
        self.highlights.highlight_square(move)

    def enable_actions(self, actions):
        self.pieces.enable_actions(actions)


class PieceSquare(Image):
    """ One square of the game board """
    def __init__(self, index, on_square_touch_down):
        super(PieceSquare, self).__init__()
        self.index = index
        self.enabled = True
        self.on_square_touch_down = on_square_touch_down

    def set_source(self, source):
        self.source = source

    def set_enabled(self, enabled):
        self.enabled = enabled

    def on_touch_down(self, touch):
        """ Call parent function if properly touched down """
        if self.enabled and self.collide_point(*touch.pos):
            self.on_square_touch_down(self.index % 7)
        return super(PieceSquare, self).on_touch_down(touch)


class PieceGrid(GridLayout):
    """ Visually represents the board's pieces on the grid """
    def __init__(self, rows, cols, on_square_touch_down):
        super(PieceGrid, self).__init__()
        self.rows = rows
        self.cols = cols
        self.on_square_touch_down = on_square_touch_down
        self.squares = {}

        self.init_board()

    def init_board(self):
        """ Create empty squares for the grid """
        board = np.zeros((self.rows, self.cols))
        self.render_board(board, self.init_square)

    def update_board(self, board):
        """ Modify the squares according to new board """
        self.render_board(board, self.update_square)

    @staticmethod
    def render_board(board, render_square):
        """
        Change the piece distribution on the grid based
        on the render_square method passed as an argument
        """
        board = np.flipud(board).reshape(-1)
        for i, v in enumerate(board):
            render_square(i, v)

    def update_square(self, index, value):
        """ Modify the content (image) of a single square """
        self.squares[index].set_source(IMG_SRC[value])

    def init_square(self, index, value):
        """ Create a square (image) and adds it to the grid """
        square = PieceSquare(index, self.on_square_touch_down)
        square.set_source(IMG_SRC[value])
        self.squares[index] = square
        self.add_widget(square)

    def enable_actions(self, actions):
        """
        Takes and array of booleans, one per column,
        and enables the squares on that column
        """
        enabled = False
        for i in self.squares:
            if actions is not None:
                enabled = actions[i % 7]
            self.squares[i].set_enabled(enabled)


class HighlightGrid(GridLayout):
    """ Layer on top of the pieces to highlight last player piece """
    def __init__(self, rows, cols):
        super(HighlightGrid, self).__init__()
        self.rows = rows
        self.cols = cols
        self.squares = {}
        self.init_board()

    def init_board(self):
        """ Create the highlight squares (images) and add them to the grid """
        for i in range(self.rows*self.cols):
            square = Image(source=IMG_SRC[0])
            self.squares[i] = square
            self.add_widget(square)

    def reset_board(self):
        """ Hide every highlight square """
        for i in range(self.rows*self.cols):
            self.squares[i].source = IMG_SRC[0]

    def highlight_square(self, index):
        """ Highlight a single square and unhighlight others """
        self.reset_board()
        if index is not None:
            self.squares[index].source = IMG_SRC['highlight']